import 'package:flutter/material.dart';

class AboutListTileExample extends StatefulWidget {
  final String title;

  AboutListTileExample(this.title);

  @override
  _AboutListTileExampleState createState() => _AboutListTileExampleState();
}

class _AboutListTileExampleState extends State<AboutListTileExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: Drawer(
        child: SingleChildScrollView(
          child: SafeArea(
            child: AboutListTile(
              icon: const Icon(
                Icons.info,
                color: Color(0xff5808e5),
              ),
              applicationName: 'Flutterer',
              applicationVersion: '1.2.3',
              applicationIcon: Icon(
                Icons.flutter_dash_rounded,
                size: 48,
                color: Color(0xff5808e5),
              ),
              applicationLegalese:
                  'Here in "applicationLegalese" you can describe your app policies, terms and conditions',
              aboutBoxChildren: [
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text(
                    "I am optional children 1",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Text(
                    "I am optional children 2",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12),
                  ),
                ),
              ],
              dense: false,
            ),
          ),
        ),
      ),
      body: Container(),
    );
  }
}
